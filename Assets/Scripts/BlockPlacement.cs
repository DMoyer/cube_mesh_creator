﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPlacement : MonoBehaviour
{
    public float Distance = 5;
    public float Deadzone = 0;
    public CreateMesh objCreateMesh;
    private bool _isFire1Clicking = false;
    private bool _isFire2Clicking = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bool isFire1 = Input.GetAxis("Fire1") > Deadzone;
        bool isFire2 = Input.GetAxis("Fire2") > Deadzone;

        if (isFire1 && !_isFire1Clicking) {
            CastPlacementRay();
            _isFire1Clicking = true;
        } else if (!isFire1 ) {
            _isFire1Clicking = false;
        }

        if (isFire2 && !_isFire2Clicking) {
            CastRemovalRay();
            _isFire2Clicking = true;
        } else if (!isFire2) {
            _isFire2Clicking = false;
        }
    }

    private void CastPlacementRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);        
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Distance*5)) {
            string hitObjName = hit.collider.gameObject.name;

            if (hitObjName.Contains("save")) {
                Debug.Log("Save");
                objCreateMesh.SavePrefab();
            } else {
            //Draw ray cast/vector and normal
            //Debug.DrawLine(ray.origin, hit.point, Color.cyan,15f);
            //Debug.DrawLine(hit.point, hit.point + (hit.normal * 1.1f), Color.red, 15f);
            
            //Add Cube to mesh
            objCreateMesh.UpdateMesh(getOriginForNewCube(hit), false);
            }
        }        
    }

    private void CastRemovalRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Distance * 5)) {
            //Draw ray cast/vector and normal
            //Debug.DrawLine(ray.origin, hit.point, Color.cyan,15f);
            //Debug.DrawLine(hit.point, hit.point + hit.normal, Color.red, 15f);
            
            //Add Cube to mesh
            objCreateMesh.UpdateMesh(getOriginOfHitCube(hit), true);
        }
    }

    private Vector3 getOriginOfHitCube(RaycastHit hit)
    {
        Vector3 origin = new Vector3((int)hit.point.x, (int)hit.point.y, (int)hit.point.z);
        //Debug.Log("Original Hit Point: " + hit.point);
        //Debug.Log("Hit Normal: " + hit.normal);

        bool isRight = hit.normal == Vector3.right;
        bool isLeft = hit.normal == Vector3.left;
        bool isBack = hit.normal == Vector3.back;
        bool isForward = hit.normal == Vector3.forward;
        bool isDown = hit.normal == Vector3.down;
        bool isUp = hit.normal == Vector3.up;

        if (isRight || isForward || isUp) {
            origin -= hit.normal;
        }

        if (origin.y < 0 && !isDown && !isUp)
            origin.y -= 1;
        if (origin.x < 0 && !isLeft && !isRight)
            origin.x -= 1;
        if (origin.z < 0 && !isBack && !isForward)
            origin.z -= 1;

        return origin;
    }

    private Vector3 getOriginForNewCube(RaycastHit hit)
    {
        Vector3 newCubePos = hit.point;
        //Debug.Log("Original Hit Point: " + newCubePos);
        //Debug.Log("Hit Normal: " + hit.normal);

        bool isRight = hit.normal == Vector3.right;
        bool isLeft = hit.normal == Vector3.left;
        bool isBack = hit.normal == Vector3.back;
        bool isForward = hit.normal == Vector3.forward;
        bool isDown = hit.normal == Vector3.down;
        bool isUp = hit.normal == Vector3.up;

        if (isLeft || isBack || isDown) {
            newCubePos += hit.normal;
        }

        if (newCubePos.y < 0 && !isDown && !isUp)
            newCubePos.y -= 1;
        if (newCubePos.x < 0 && !isLeft && !isRight)
            newCubePos.x -= 1;
        if (newCubePos.z < 0 && !isBack && !isForward)
            newCubePos.z -= 1;

        return newCubePos;
    }
}
