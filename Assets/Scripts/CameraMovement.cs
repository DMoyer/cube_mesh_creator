﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class CameraMovement : MonoBehaviour
{
    public GameObject parent;
    [SerializeField] private MouseLook m_MouseLook;

    // Start is called before the first frame update
    void Start()
    {
        m_MouseLook = new MouseLook();
        m_MouseLook.Init(parent.transform, transform);
    }

    // Update is called once per frame
    void Update()
    {
        m_MouseLook.LookRotation(parent.transform, transform);
    }
}
