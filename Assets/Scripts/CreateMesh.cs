﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class CreateMesh : MonoBehaviour
{
    public MeshFilter meshFilter;
    public MeshCollider meshCollider;


    private MeshData customMesh;
    
    // Start is called before the first frame update
    void Start()
    {
        customMesh = new MeshData(1, 1);
        customMesh.AddCube(Vector3.zero);
        WriteMesh(customMesh.CreateMesh());        
    }

    private void WriteMesh(Mesh m)
    {
        meshFilter.sharedMesh = m;
        meshCollider.sharedMesh = m;
    }

    public void UpdateMesh(Vector3 newCube, bool shouldRemoveCube)
    {
        try {
            Vector3 translate = new Vector3((int)newCube.x, (int)newCube.y, (int)newCube.z);
            Debug.Log("Cube Location: " + translate);

            if (shouldRemoveCube)
            {
                customMesh.RemoveCube(translate);

            } else
            {
                customMesh.AddCube(translate);
            }
            
            WriteMesh(customMesh.CreateMesh());
        } catch {
            Debug.Log("Unable to convert newCube Location to Ints");
        }
    }

    public void SaveMesh() {
        string datetimeStr = DateTime.Now.ToString("MMddyyyy_HHmmss");
        AssetDatabase.CreateAsset(customMesh.CreateMesh(), "Assets/MyGeneratedMesh" + datetimeStr + ".asset");
        AssetDatabase.SaveAssets();
    }

    public void SavePrefab()
    {
        string datetimeStr = DateTime.Now.ToString("MMddyyyy_HHmmss");
        GameObject newObj = new GameObject();
        Mesh m = customMesh.CreateMesh();

        newObj.AddComponent<MeshFilter>().mesh = m;
        newObj.AddComponent<MeshCollider>().sharedMesh = m;
        newObj.AddComponent<MeshRenderer>();

        AssetDatabase.CreateAsset(m, "Assets/SavedMeshes/MyGeneratedMesh_" + datetimeStr + ".asset");
        
        var prefab = EditorUtility.CreateEmptyPrefab("Assets/SavedPrefabs/NewObj_" + datetimeStr + ".prefab");
        EditorUtility.ReplacePrefab(newObj, prefab);
        AssetDatabase.Refresh();
    }
}

public class MeshData
{
    public List<Vector3> verticies;
    public List<int> triangles;
    public List<Vector2> uvs;
    
    private SortedSet<Cube> cubes;

    int triangleIndex;
    int _width;
    int _height;

    public MeshData(int width, int height)
    {
        cubes = new SortedSet<Cube>();
        InitializeMeshData();

        _width = width;
        _height = height;
        triangleIndex = 0;
    }

    private void InitializeMeshData()
    {
        verticies = new List<Vector3>();
        triangles = new List<int>();
        uvs = new List<Vector2>();
    }

    private void AddAllCubeData()
    {
        foreach(Cube c in cubes) {
            c.AddAllSides(ref verticies,ref triangles,ref uvs);
        }
    }

    private void AddLimitedCubeData()
    {
        foreach (Cube c in cubes)
        {
            //Side 1
            if (!cubes.Contains(new Cube(c.Origin+Vector3.down)))
                c.AddBottom(ref verticies, ref triangles, ref uvs);
            //Side 2
            if (!cubes.Contains(new Cube(c.Origin+Vector3.left)))
                c.AddLeft(ref verticies, ref triangles, ref uvs);
            //Side 3
            if(!cubes.Contains(new Cube(c.Origin+Vector3.back)))
                c.AddFront(ref verticies, ref triangles, ref uvs);
            //Side 4
            if(!cubes.Contains(new Cube(c.Origin+Vector3.right)))
                c.AddRight(ref verticies, ref triangles, ref uvs);
            //Side 5
            if(!cubes.Contains(new Cube(c.Origin+Vector3.forward)))
                c.AddBack(ref verticies, ref triangles, ref uvs);
            //Side 6
            if(!cubes.Contains(new Cube(c.Origin+Vector3.up)))
                c.AddTop(ref verticies, ref triangles, ref uvs);
        }
    }

    public void AddCube(Vector3 translate)
    {
        Cube newCube = new Cube(translate);
        if (!cubes.Contains(newCube))
        {
            cubes.Add(newCube);
        } else
        {
            Debug.Log("Cube already exists in Mesh!");
        }        
    }

    public void RemoveCube(Vector3 cubeLocation)
    {
        Cube existingCube = new Cube(cubeLocation);
        cubes.Remove(existingCube);
    }

    public Mesh CreateMesh()
    {
        InitializeMeshData();
        //AddAllCubeData();
        AddLimitedCubeData();
        Mesh mesh = new Mesh();
        mesh.vertices = verticies.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();
        return mesh;
    }

}

public class Cube:IComparable<Cube>
{
    public Vector3 Origin { get; }

    private Vector3 vertexA, vertexB, vertexC, vertexD, vertexE, vertexF, vertexG, vertexH;

    public Cube(Vector3 translate)
    {
        Origin = translate;

        vertexA = Origin;
        vertexB = Origin + Vector3.right;
        vertexC = Origin + Vector3.forward;
        vertexD = Origin + Vector3.right + Vector3.forward;
        vertexE = Origin + Vector3.up;
        vertexF = vertexE + Vector3.right;
        vertexG = vertexE + Vector3.forward;
        vertexH = vertexE + Vector3.right + Vector3.forward;
    }

    //X-Axis Sides
    public void AddLeft(ref List<Vector3> verticies, ref List<int> triangles, ref List<Vector2> uvs)
    {
        List<Vector3> cubeVerticies = new List<Vector3>() { vertexA, vertexC, vertexE, vertexG };
        int index = verticies.Count;

        //Add Verticies 
        verticies.AddRange(cubeVerticies);
        
        //Side 2 - Left
        triangles.AddRange(new[] { index + 2, index, index + 1 });
        triangles.AddRange(new[] { index + 2, index + 1, index + 3 });
    }
    public void AddRight(ref List<Vector3> verticies, ref List<int> triangles, ref List<Vector2> uvs)
    {
        List<Vector3> cubeVerticies = new List<Vector3>() { vertexB, vertexD, vertexF, vertexH };
        int index = verticies.Count;

        //Add Verticies 
        verticies.AddRange(cubeVerticies);
        
        //Side 4 - Right
        triangles.AddRange(new[] { index + 3, index + 1, index });
        triangles.AddRange(new[] { index + 3, index, index + 2 });
    }

    //Y-Axis Sides
    public void AddTop(ref List<Vector3> verticies, ref List<int> triangles, ref List<Vector2> uvs)
    {
        List<Vector3> cubeVerticies = new List<Vector3>() { vertexE, vertexF, vertexG, vertexH };
        int index = verticies.Count;

        //Add Verticies 
        verticies.AddRange(cubeVerticies);

        //Side 6 - Top
        triangles.AddRange(new[] { index + 1, index, index + 2 });
        triangles.AddRange(new[] { index + 1, index + 2, index + 3 });
    }
    public void AddBottom(ref List<Vector3> verticies, ref List<int> triangles, ref List<Vector2> uvs)
    {
        List<Vector3> cubeVerticies = new List<Vector3>() { vertexA, vertexB, vertexC, vertexD };
        int index = verticies.Count;

        //Add Verticies 
        verticies.AddRange(cubeVerticies);

        //Side 1 - Bottom
        triangles.AddRange(new[] { index, index + 1, index + 3 });
        triangles.AddRange(new[] { index, index + 3, index + 2 });
    }

    //Z-Axis Sides
    public void AddFront(ref List<Vector3> verticies, ref List<int> triangles, ref List<Vector2> uvs)
    {
        List<Vector3> cubeVerticies = new List<Vector3>() { vertexA, vertexB, vertexE, vertexF };
        int index = verticies.Count;

        //Add Verticies 
        verticies.AddRange(cubeVerticies);

        //Side 3 - Front
        triangles.AddRange(new[] { index + 3, index + 1, index });
        triangles.AddRange(new[] { index + 3, index, index + 2 });
    }
    public void AddBack(ref List<Vector3> verticies, ref List<int> triangles, ref List<Vector2> uvs)
    {
        List<Vector3> sideVerticies = new List<Vector3>() { vertexC, vertexD, vertexG, vertexH };
        int index = verticies.Count;

        //Add Verticies 
        verticies.AddRange(sideVerticies);

        //Side 5 - Back
        triangles.AddRange(new[] { index + 2, index, index + 1 });
        triangles.AddRange(new[] { index + 2, index + 1, index + 3 });
    }

    public void AddAllSides(ref List<Vector3> verticies, ref List<int> triangles, ref List<Vector2> uvs)
    {
        AddBottom(ref verticies, ref triangles, ref uvs);
        AddLeft(ref verticies, ref triangles, ref uvs);
        AddFront(ref verticies, ref triangles, ref uvs);
        AddRight(ref verticies, ref triangles, ref uvs);
        AddBack(ref verticies, ref triangles, ref uvs);
        AddTop(ref verticies, ref triangles, ref uvs);

        //This is temporary until I figure this out
        uvs.AddRange(new[] {new Vector2(0, 0.66f),
                            new Vector2(0.25f, 0.66f),
                            new Vector2(0, 0.33f),
                            new Vector2(0.25f, 0.33f),

                            new Vector2(0.5f, 0.66f),
                            new Vector2(0.5f, 0.33f),
                            new Vector2(0.75f, 0.66f),
                            new Vector2(0.75f, 0.33f),

                            new Vector2(0, 0.66f),
                            new Vector2(0.25f, 0.66f),
                            new Vector2(0, 0.33f),
                            new Vector2(0.25f, 0.33f),

                            new Vector2(0.5f, 0.66f),
                            new Vector2(0.5f, 0.33f),
                            new Vector2(0.75f, 0.66f),
                            new Vector2(0.75f, 0.33f),

                            new Vector2(0, 0.66f),
                            new Vector2(0.25f, 0.66f),
                            new Vector2(0, 0.33f),
                            new Vector2(0.25f, 0.33f),

                            new Vector2(0.5f, 0.66f),
                            new Vector2(0.5f, 0.33f),
                            new Vector2(0.75f, 0.66f),
                            new Vector2(0.75f, 0.33f)});
    }

    public int CompareTo(Cube other)
    {
        Vector3 otherOrigin = other.Origin;

        if (Origin.x < otherOrigin.x)
            return -1;
        else if (Origin.x > otherOrigin.x)
            return 1;
        else
        {
            if (Origin.z < otherOrigin.z)
                return -1;
            else if (Origin.z > otherOrigin.z)
                return 1;
            else
            {
                if (Origin.y < otherOrigin.y)
                    return -1;
                else if (Origin.y > otherOrigin.y)
                    return 1;
                else
                    return 0;
            }
        }
        throw new NotImplementedException();
    }
}

