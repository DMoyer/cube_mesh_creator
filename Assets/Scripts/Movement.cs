﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed;
    public float runMulptiplier;
    public float smooth;
    public float deadzone = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float hor = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");
        float fly = Input.GetAxis("Fly");
        float movementSpeed = speed;

        if (Input.GetAxis("Run") > 0)
            movementSpeed *= runMulptiplier;

        //Debug.Log("Hor: " + hor.ToString() + ", Vert:" + vert.ToString() + ", Fly:" + fly.ToString());
        
        Vector3 currentPosition = transform.position;

        if (hor < -deadzone)
            currentPosition += (RotateDirection(Vector3.left) * hor * -1 * movementSpeed);
        if (hor > deadzone)
            currentPosition += (RotateDirection(Vector3.right) * hor * movementSpeed);
        
        if (vert < -deadzone)
            currentPosition += (RotateDirection(Vector3.back) * vert * -1 * movementSpeed);
        if (vert > deadzone)
            currentPosition += (RotateDirection(Vector3.forward) * vert * movementSpeed);

        if (fly < -deadzone)
            currentPosition += (Vector3.down * fly * -1 * movementSpeed);
        if (fly > deadzone)
            currentPosition += (Vector3.up * fly * movementSpeed);

        transform.position = Vector3.Lerp(transform.position, currentPosition, smooth);

    }

    private Vector3 RotateDirection(Vector3 dir)
    {
        Vector3 result = transform.rotation * dir;
        return result;
    }
}
