﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public bool Use_Test_Rotation = false;
    public Quaternion testRotation;

    private Quaternion OriginalRotation;
    // Start is called before the first frame update
    void Start()
    {
        OriginalRotation = gameObject.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Use_Test_Rotation)
        {
            transform.rotation = testRotation;
        }
    }
}
